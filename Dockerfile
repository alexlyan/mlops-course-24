# Используйте официальный образ Python как базовый
FROM python:3.11-slim

# Установка зависимостей системы, необходимых для poetry
RUN apt-get update \
    && apt-get install -y curl \
    && rm -rf /var/lib/apt/lists/*

# Установка Poetry
RUN curl -sSL https://install.python-poetry.org | python3 - \
    && echo "export PATH=/root/.local/bin:$PATH" >> $HOME/.bashrc

# Добавление /root/.local/bin в PATH напрямую через Dockerfile
ENV PATH="/root/.local/bin:${PATH}"

# Копирование файлов проекта в образ
WORKDIR /app
COPY pyproject.toml poetry.lock* /app/

# Установка зависимостей проекта
RUN poetry config virtualenvs.create false \
    && poetry install --no-dev --no-interaction --no-ansi

# Копирование остальных файлов проекта
COPY . /app

ENTRYPOINT ["python"]
CMD ["--help"]
